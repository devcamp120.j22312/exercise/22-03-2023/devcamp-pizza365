import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import logoBrand from './assets/images/pizza-logo-58188.jpg';
import pizzaCarouselImg from './assets/images/1.jpg';
import pizzaHawaii from './assets/images/hawaiian.jpg';
import pizzaBacon from './assets/images/bacon.jpg';
import pizzaSeafood from './assets/images/seafood.jpg';

function App() {
  return (
    <div>
      <div className="div-header">
        <div className="text-header">
          Trang Chủ
        </div>
        <div className="text-header">
          Combo
        </div>
        <div className="text-header">
          Loại Pizza
        </div>
        <div className="text-header">
          Gửi Đơn Hàng
        </div>
      </div>

      <div className="container">
        <div>
          <img className="img-brand" src={logoBrand} alt="logo-img" />
        </div>
        <div className="div-brand">
          <h4>PIZZA 365</h4>
          <p>Truly Italian !</p>
        </div>

        <div>
          <div className="div-img-carousel">
            <img src={pizzaCarouselImg} slt="First slide" />
            <div className="div-description">
              <h5>New Dishes</h5>
              <p>TUNA PIZZA</p>
            </div>
          </div>
        </div>

        <div className="card-header text-center">
          <h3>TẠI SAO LẠI LÀ PIZZA 365</h3>
          <hr className="line" />
        </div>
        <div className="div-card-deck">
          <div className="card" style={{ backgroundColor: "lightgoldenrodyellow" }}>
            <div className="card-body">
              <h4>Đa dạng</h4>
              <p>Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot hiện nay </p>
            </div>
          </div>
          <div className="card" style={{ backgroundColor: 'yellow' }}>
            <div className="card-body">
              <h4 className="card-title">Chất lượng</h4>
              <p className="card-text">Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</p>
            </div>
          </div>
          <div className="card" style={{ backgroundColor: "lightsalmon" }}>
            <div className="card-body">
              <h4 className="card-title">Hương vị</h4>
              <p className="card-text">Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza365</p>
            </div>
          </div>
          <div className="card" style={{ backgroundColor: 'orange' }}>
            <div className="card-body">
              <h4 className="card-title">Dịch vụ</h4>
              <p className="card-text">Nhân viên thân thiện, nhà hàng hiện đại, dich vụ giao hàng nhanh, chất lượng</p>
            </div>
          </div>
        </div>

        <div className="card-header text-center">
          <h3>CHỌN SIZE PIZZA</h3>
          <hr className="line" />
          <p>Chọn combo pizza phù hợp với nhu cầu của bạn.</p>
        </div>
        <div className="div-combo-card-deck">
          <div className="card div-card-combo">
            <div className="card-header-combo text-center">
              <h3>S (small)</h3>
            </div>
            <div className="card-body text-center">
              <p className="card-text">Đường kính: <span className="font-weight-bold"> 20cm</span></p>
              <hr />
              <p className="card-text">Sườn nướng: <span className="font-weight-bold"> 2</span></p>
              <hr />
              <p className="card-text">Salad: <span className="font-weight-bold"> 200g</span></p>
              <hr />
              <p className="card-text">Nước ngọt: <span className="font-weight-bold"> 2</span></p>
              <hr />
              <h2>150.000</h2>
              <p>VNĐ</p>
            </div>
            <button className="btn btn-warning btn-choose" id="btn-small">Chọn</button>
          </div>
          <div className="card div-card-combo">
            <div className="card-header-combo text-center">
              <h3>M (medium)</h3>
            </div>
            <div className="card-body text-center">
              <p className="card-text">Đường kính: <span className="font-weight-bold"> 25cm</span></p>
              <hr />
              <p className="card-text">Sườn nướng: <span className="font-weight-bold"> 4</span></p>
              <hr />
              <p className="card-text">Salad: <span className="font-weight-bold"> 300g</span></p>
              <hr />
              <p className="card-text">Nước ngọt: <span className="font-weight-bold"> 3</span></p>
              <hr />
              <h2>200.000</h2>
              <p>VNĐ</p>
            </div>
            <button className="btn btn-warning btn-choose" id="btn-medium">Chọn</button>
          </div>
          <div className="card div-card-combo">
            <div className="card-header-combo text-center">
              <h3>L (large)</h3>
            </div>
            <div className="card-body text-center">
              <p className="card-text">Đường kính: <span className="font-weight-bold"> 30cm</span></p>
              <hr />
              <p className="card-text">Sườn nướng: <span className="font-weight-bold"> 8</span></p>
              <hr />
              <p className="card-text">Salad: <span className="font-weight-bold"> 500g</span></p>
              <hr />
              <p className="card-text">Nước ngọt: <span className="font-weight-bold"> 4</span></p>
              <hr />
              <h2>250.000</h2>
              <p>VNĐ</p>
            </div>
            <button className="btn btn-warning btn-choose" id="btn-large">Chọn</button>
          </div>
        </div>
      </div>

      <div className="card-header text-center">
        <h3>CHỌN LOẠI PIZZA</h3>
        <hr className="line" />
      </div>
      <div className="div-pizza-select">
        <div className="card div-card-pizza">
          <div className="card-header img-header">
            <img className="div-img" src={pizzaSeafood} alt="pizza seafood" />
          </div>
          <div className="card-body">
            <h3>OCEAN MANIA</h3>
            <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
            <p>Xốt cà chua, Phô mai Mozzarella, Tôm, Mực, Thanh cua, Hành tây</p>
          </div>
          <button className="btn btn-warning btn-choose" id="btn-mania">Chọn</button>
        </div>
        <div className="card div-card-pizza">
          <div className="card-header img-header">
            <img className="div-img" src={pizzaHawaii} alt="pizza seafood" />
          </div>
          <div className="card-body">
            <h3>HAWAIIAN</h3>
            <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
            <p>Xốt cà chua, Phô mai Mozzarella, Thịt dăm bông, Thơm</p>
          </div>
          <button className="btn btn-warning btn-choose" id="btn-hawaii">Chọn</button>
        </div>
        <div className="card div-card-pizza">
          <div className="card-header img-header">
            <img className="div-img" src={pizzaBacon} alt="pizza seafood" />
          </div>
          <div className="card-body">
            <h3>BACON</h3>
            <p>PIZZA BACON</p>
            <p>Xốt cà chua, Phô mai Mozzarella, Bacon</p>
          </div>
          <button className="btn btn-warning btn-choose" id="btn-bacon">Chọn</button>
        </div>
      </div>

      <div className="card-header text-center">
        <h3>CHỌN ĐỒ UỐNG</h3>
        <hr className="line" />
      </div>
      <div className="card select-drink">
        <select className="form-control">
          <option>Chọn nước uống</option>
          <option>Cocacola</option>
          <option>Pepsi</option>
          <option>Aquafina</option>
          <option>Trà tắc</option>
        </select>
      </div>

      <div className="card-header text-center ">
            <h3 >GỬI ĐƠN HÀNG</h3>
            <hr className="line"/>
        </div>
        <div className="card user-infor ">
          <div className="row user-order">
            <label>Tên</label>
            <input placeholder="Họ Tên"></input>
          </div>
          <div className="row user-order">
            <label>Email</label>
            <input placeholder="Email"></input>
          </div>
          <div className="row user-order">
            <label>Điện thoại</label>
            <input placeholder="Điện Thoại"></input>
          </div>
          <div className="row user-order">
            <label>Địa chỉ</label>
            <input placeholder="Địa chỉ"></input>
          </div>
          <div className="row user-order">
            <label>Mã giảm giá</label>
            <input placeholder="Mã giảm giá"></input>
          </div>
          <div className="row user-order">
            <label>Ghi chú</label>
            <input placeholder="Ghi chú"></input>
          </div>
          <div className="row user-order">
            <button className="btn btn-warning btn-choose">Gửi</button>
          </div>
        </div>

        <div className ="div-footer">
          <div>
            <h3 >
              Footer
            </h3>
          </div>
              <div >
                <button class=" btn btn-dark">To the top</button>
              </div>
              <div>
                <i class="fa fa-facebook-official w3-hover-opacity"></i>
                <i class="fa fa-instagram w3-hover-opacity"></i>
                <i class="fa fa-snapchat w3-hover-opacity"></i>
                <i class="fa fa-pinterest-p w3-hover-opacity"></i>
                <i class="fa fa-twitter w3-hover-opacity"></i>
              </div>
              <div>
                <p>
                  Powered by DEVCAMP
                </p>
              </div>
      </div>

    </div>
  );
}

export default App;
